/* eslint-disable react-native/no-inline-styles */
import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Toast, {BaseToast} from 'react-native-toast-message';

import SplashScreen from './src/screens/SplashScreen';
import AuthNavigation from './src/navigation/AuthNavigation';
// import HomeScreenRoute from './src/navigation/HomeScreenRoute';
// import ForgotPasswordScreen from './src/screen/forgotPasswordScreen';

const Stack = createStackNavigator();

const toastConfig = {
  success: ({props, ...rest}) => (
    <BaseToast
      {...rest}
      style={{borderLeftColor: 'green'}}
      contentContainerStyle={{paddingHorizontal: 15}}
      text1Style={{
        fontSize: 18,
        fontWeight: 'bold',
      }}
      text2Style={{
        fontSize: 14,
        fontWeight: '500',
      }}
    />
  ),
};

const App = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Auth"
            component={AuthNavigation}
            options={{headerShown: false}}
          />
          {/* <Stack.Screen
            name="HomeScreenRoute"
            component={HomeScreenRoute}
            options={{headerShown: false}}
          /> */}
        </Stack.Navigator>
      </NavigationContainer>
      <Toast config={toastConfig} />
    </>
  );
};

export default App;
