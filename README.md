# ReactNativeBase

## Môi trường cần chuẩn bị

Cần setup trước môi trường để chạy React Native (Mac hoặc Win)
[setup](https://reactnative.dev/docs/environment-setup)

## Hướng dẫn cài đặt

- Clone dự án về, chạy npm install để cài các thư viện
- Chạy react-native run-android hoặc react-native run-ios để start project

## Một số lưu ý khi sử dụng

- Các file tĩnh như ảnh, font, icons để chung vào thư mục assets, phân chia cụ thể theo từng loại
- Các components chung được sử dụng ở nhiều nơi viết chung vào commons, tránh đặt tên trùng với components đã có của React Native
- Khai báo các biến constants vào folder constants, không để dưới dạng magic number
- Phân chia navigation được xử lí trong folder navigation
- Các màn hình được viết trong screens
- Việc gọi api xử lí trong folder services
