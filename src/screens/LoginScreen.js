/* eslint-disable react-native/no-inline-styles */
import React, {useRef, useMemo, useCallback} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableWithoutFeedback,
  SafeAreaView,
  Text,
} from 'react-native';
import {Button} from 'react-native-elements';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useFocusEffect} from '@react-navigation/native';

import Messages from '@/constants/Message';
import TextInputCustom from '@/commons/TextInput';
import ScreenSize from '@/constants/ScreenSize';
import {Column} from '@/commons/Layout';
import Colors from '@/constants/Color';
// import {login} from '../services/auth';

const validateSchema = Yup.object().shape({
  password: Yup.string().required(Messages.ERROR_REQUIRED),
  email: Yup.string().required(Messages.ERROR_REQUIRED),
});

const LoginScreen = ({navigation}) => {
  const refPassword = useRef(null);
  const refEmail = useRef(null);
  const formRef = useRef(null);

  const initialValues = useMemo(
    () => ({
      email: '',
      password: '',
    }),
    [],
  );

  useFocusEffect(
    useCallback(() => {
      formRef.current && formRef.current.resetForm();
    }, []),
  );

  const handleSubmitEditingEmail = useCallback(() => {
    refPassword.current?.focus();
  }, [refPassword]);

  const handleLogin = useCallback(
    async data => {
      //   const res = await login(data);
      //   if (res) {
      navigation.replace('HomeScreenRoute');
    },
    // },
    [navigation],
  );

  return useMemo(
    () => (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#ffffff',
        }}>
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          extraHeight={120}
          persistentScrollbar={false}
          style={{height: '100%'}}
          nestedScrollEnabled={true}>
          <Image
            source={require('@/assets/images/logo.png')}
            style={{
              height: 300,
              resizeMode: 'contain',
              marginTop: 30,
              top: '-5%',
              left: '20%',
            }}
          />
          <TouchableWithoutFeedback>
            <View style={{top: '-10%'}}>
              <Formik
                innerRef={formRef}
                initialValues={initialValues}
                validationSchema={validateSchema}
                onSubmit={handleLogin}>
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  touched,
                  setFieldValue,
                }) => (
                  <Column
                    style={{
                      paddingTop: ScreenSize.DEVICE_HEIGHT / 20,
                    }}
                    wrap={false}>
                    <View
                      style={{
                        margin: ScreenSize.DEVICE_HEIGHT / 30,
                        flexDirection: 'column',
                        flexWrap: 'nowrap',
                      }}>
                      <TextInputCustom
                        style={styles.input}
                        ref={refEmail}
                        error={!!(errors.email && touched.email)}
                        showError={true}
                        messageError={errors.email}
                        placeholder="Email"
                        onChangeText={handleChange('email')}
                        value={values.email}
                        returnKeyType="next"
                        onBlur={handleBlur('email')}
                        onSubmitEditing={handleSubmitEditingEmail}
                        autoCapitalize="none"
                      />
                      <TextInputCustom
                        style={styles.input}
                        secureTextEntry={true}
                        ref={refPassword}
                        error={!!(errors.password && touched.password)}
                        showError={true}
                        messageError={errors.password}
                        placeholder="Mật khẩu"
                        onChangeText={handleChange('password')}
                        value={values.password}
                        returnKeyType="next"
                        onBlur={handleBlur('password')}
                        autoCapitalize="none"
                      />

                      <View style={styles.buttonLoginArea}>
                        <Button
                          height={70}
                          title="Đăng nhập"
                          width={'100%'}
                          style={styles.buttonLogin}
                          onPress={handleSubmit}
                        />
                      </View>
                      <Text
                        style={styles.registerTextStyle}
                        onPress={() =>
                          navigation.navigate('ForgotPasswordScreen')
                        }>
                        Quên mật khẩu?
                      </Text>
                      <Text
                        style={styles.registerTextStyle}
                        onPress={() => navigation.navigate('RegisterScreen')}>
                        Chưa có tài khoản? Đăng ký ngay
                      </Text>
                    </View>
                  </Column>
                )}
              </Formik>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    ),
    [handleLogin, initialValues, handleSubmitEditingEmail, navigation],
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  input: {
    marginTop: 20,
  },
  buttonLoginArea: {
    marginVertical: 16,
  },
  buttonLogin: {
    borderRadius: 20,
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: Colors.PRIMARY,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.PRIMARY,
    paddingHorizontal: 8,
  },
  buttonLoginTitle: {
    color: Colors.WHITE,
    fontSize: 16,
    fontWeight: 'bold',
  },
  registerTextStyle: {
    color: Colors.PRIMARY,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'center',
    padding: 10,
  },
});
