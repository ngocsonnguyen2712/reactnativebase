const Colors = {
  RED: '#ff0000',
  GREY: '#808080',
  PRIMARY: '#406BB8',
  GRAY: '#707070',
  WHITE: '#FFFFFF',
  BLUE_DARK: '#243B66',
};

export default Colors;
