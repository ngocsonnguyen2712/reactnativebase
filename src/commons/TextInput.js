/* eslint-disable no-sparse-arrays */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useCallback, useMemo, useEffect} from 'react';
import {
  Animated,
  Easing,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '@/constants/Color';

const CustomInput = React.forwardRef(
  (
    {
      allowFontScaling,
      autoCapitalize,
      autoFocus,
      blurOnSubmit,
      caretHidden,
      contextMenuHidden,
      defaultValue,
      dotRequire = true,
      editable = true,
      error = false,
      keyboardType,
      maxLength,
      messageError,
      multiline,
      numberOfLines,
      onBlur,
      onChange,
      onChangeText,
      onEndEditing,
      onFocus,
      onSubmitEditing,
      placeholder,
      placeholderTextColor,
      returnKeyType,
      secureTextEntry,
      showError,
      style,
      styleInput,
      textAlign,
      value,
      icon,
      handlePress,
    },
    forwardRef,
  ) => {
    const animatedValue = React.useRef(new Animated.Value(0)).current;
    const [isFocus, setIsFocus] = useState(false);
    const [itemHeight, setItemHeight] = useState(0);
    const [itemWidth, setItemWidth] = useState(0);

    const animation = useCallback(
      toValue => {
        return Animated.timing(animatedValue, {
          duration: 200,
          toValue,
          easing: Easing.ease,
          useNativeDriver: true,
        });
      },
      [animatedValue],
    );

    const handleFocus = useCallback(
      e => {
        animation(1).start();
        setIsFocus(true);
        onFocus && onFocus(e);
      },
      [animation, onFocus],
    );

    const handleBlur = useCallback(
      e => {
        setIsFocus(false);
        if (!value) {
          animation(0).start();
        }
        onBlur && onBlur(e);
      },
      [animation, value, onBlur],
    );

    const onParentLayout = useCallback(event => {
      const {layout} = event.nativeEvent;
      setItemHeight(layout.height);
    }, []);

    const onChildLayout = useCallback(event => {
      const {layout} = event.nativeEvent;
      setItemWidth(layout.width);
    }, []);

    const translateY = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -(itemHeight / 2)],
    });

    const translateX = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -(itemWidth - itemWidth * 0.8) / 2],
    });

    const scale = animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0.8],
    });

    const colorFilter = useMemo(() => {
      if (isFocus && error) {
        return Colors.RED;
      } else if (isFocus && !error) {
        return Colors.PRIMARY;
      } else if (!isFocus && error) {
        return Colors.RED;
      } else {
        return Colors.GRAY;
      }
    }, [error, isFocus]);

    useEffect(() => {
      if (value) {
        animation(1).start();
      }
    }, [animation, value]);
    return useMemo(
      () => (
        <React.Fragment>
          <View
            onLayout={onParentLayout}
            style={[
              {
                borderRadius: 6,
                borderWidth: isFocus && !error ? 1 : 0.5,
                borderColor: colorFilter,
                height: 50,
                width: '100%',
                justifyContent: 'center',
                paddingHorizontal: 10,
              },
              style,
              ,
            ]}>
            <View>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                }}>
                <Animated.View
                  onLayout={onChildLayout}
                  style={{
                    transform: [
                      {
                        translateY,
                      },
                      {
                        translateX,
                      },
                      {
                        scale,
                      },
                    ],
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      backgroundColor: '#fff',
                      color: colorFilter,
                      fontSize: 14,
                    }}>
                    {placeholder}
                    {dotRequire && (
                      <Text style={{color: 'red', fontSize: 14}}> *</Text>
                    )}
                  </Text>
                </Animated.View>
                <TextInput
                  allowFontScaling={allowFontScaling}
                  autoCapitalize={autoCapitalize}
                  autoFocus={autoFocus}
                  blurOnSubmit={blurOnSubmit}
                  caretHidden={caretHidden}
                  contextMenuHidden={contextMenuHidden}
                  defaultValue={defaultValue}
                  editable={editable}
                  keyboardType={keyboardType}
                  maxLength={maxLength}
                  multiline={multiline}
                  numberOfLines={numberOfLines}
                  onChange={onChange}
                  onEndEditing={onEndEditing}
                  placeholderTextColor={placeholderTextColor}
                  returnKeyType={returnKeyType}
                  secureTextEntry={secureTextEntry}
                  ref={forwardRef}
                  onChangeText={onChangeText}
                  textAlign={textAlign}
                  value={value}
                  style={[
                    {
                      fontSize: 16,
                      padding: 0,
                      height: '250%',
                      position: 'absolute',
                      left: 0,
                      right: 0,
                      top: '-70%',
                      bottom: 0,
                    },
                    styleInput,
                  ]}
                  onFocus={handleFocus}
                  onBlur={handleBlur}
                  onSubmitEditing={onSubmitEditing}
                />
                {icon && (
                  <TouchableOpacity
                    onPress={handlePress}
                    style={{position: 'absolute', right: '2%', bottom: '5%'}}>
                    {icon}
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
          {error && showError && (
            <Text
              style={{
                color: Colors.RED,
                marginTop: 4,
                lineHeight: 20,
                zIndex: -1,
              }}>
              {messageError}
            </Text>
          )}
        </React.Fragment>
      ),
      [
        allowFontScaling,
        autoCapitalize,
        autoFocus,
        blurOnSubmit,
        caretHidden,
        colorFilter,
        contextMenuHidden,
        defaultValue,
        dotRequire,
        editable,
        error,
        messageError,
        showError,
        handleBlur,
        handleFocus,
        isFocus,
        keyboardType,
        maxLength,
        multiline,
        numberOfLines,
        onChange,
        onChangeText,
        onChildLayout,
        onParentLayout,
        onEndEditing,
        onSubmitEditing,
        placeholder,
        placeholderTextColor,
        returnKeyType,
        translateX,
        translateY,
        scale,
        secureTextEntry,
        style,
        styleInput,
        textAlign,
        value,
        forwardRef,
        icon,
        handlePress,
      ],
    );
  },
);
export default CustomInput;
